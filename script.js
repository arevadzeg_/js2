let university = {
  credits: {
    javascript: 4,
    react: 7,
    python: 6,
    java: 3,
  },
  gpa: {
    0.5: 0.5,
    1: 1,
    2: 2,
    3: 3,
    4: 4,
  },
};

students = [];

students[0] = {
  firstName: "Jean",
  lastName: "Reno",
  age: 26,
  scores: {
    frontEnd: {
      javascript: 62,
      react: 57,
    },
    backEnd: {
      python: 88,
      java: 90,
    },
  },
};

students[1] = {
  firstName: "Claud",
  lastName: "Monet",
  age: 19,
  scores: {
    frontEnd: {
      javascript: 77,
      react: 52,
    },
    backEnd: {
      python: 92,
      java: 67,
    },
  },
};

students[2] = {
  firstName: "Van",
  lastName: "Gogh",
  age: 21,
  scores: {
    frontEnd: {
      javascript: 51,
      react: 98,
    },
    backEnd: {
      python: 65,
      java: 70,
    },
  },
};

students[3] = {
  firstName: "Dam",
  lastName: "Squares",
  age: 36,
  scores: {
    frontEnd: {
      javascript: 82,
      react: 53,
    },
    backEnd: {
      python: 80,
      java: 65,
    },
  },
};

let statistics = [];

(statistics[0] = {
  sumOfScores:
    students[0].scores.frontEnd.javascript +
    students[0].scores.frontEnd.react +
    students[0].scores.backEnd.java +
    students[0].scores.backEnd.python,
  averageScores:
    (students[0].scores.frontEnd.javascript +
      students[0].scores.frontEnd.react +
      students[0].scores.backEnd.java +
      students[0].scores.backEnd.python) /
    4,
  GPA:
    (university.credits.javascript * university.gpa[1] +
      university.credits.react * university.gpa[0.5] +
      university.credits.python * university.gpa[3] +
      university.credits.java * university.gpa[3]) /
    (university.credits.javascript +
      university.credits.python +
      university.credits.java +
      university.credits.react),
}),
  (statistics[1] = {
    sumOfScores:
      students[1].scores.frontEnd.javascript +
      students[1].scores.frontEnd.react +
      students[1].scores.backEnd.java +
      students[1].scores.backEnd.python,
    averageScores:
      (students[1].scores.frontEnd.javascript +
        students[1].scores.frontEnd.react +
        students[1].scores.backEnd.java +
        students[1].scores.backEnd.python) /
      4,
    GPA:
      (university.credits.javascript * university.gpa[2] +
        university.credits.react * university.gpa[0.5] +
        university.credits.python * university.gpa[4] +
        university.credits.java * university.gpa[2]) /
      (university.credits.javascript +
        university.credits.python +
        university.credits.java +
        university.credits.react),
  }),
  (statistics[2] = {
    sumOfScores:
      students[2].scores.frontEnd.javascript +
      students[2].scores.frontEnd.react +
      students[2].scores.backEnd.java +
      students[2].scores.backEnd.python,
    averageScores:
      (students[2].scores.frontEnd.javascript +
        students[2].scores.frontEnd.react +
        students[2].scores.backEnd.java +
        students[2].scores.backEnd.python) /
      4,
    GPA:
      (university.credits.javascript * university.gpa[0.5] +
        university.credits.react * university.gpa[4] +
        university.credits.python * university.gpa[1] +
        university.credits.java * university.gpa[1]) /
      (university.credits.javascript +
        university.credits.python +
        university.credits.java +
        university.credits.react),
  }),
  (statistics[3] = {
    sumOfScores:
      students[3].scores.frontEnd.javascript +
      students[3].scores.frontEnd.react +
      students[3].scores.backEnd.java +
      students[3].scores.backEnd.python,
    averageScores:
      (students[3].scores.frontEnd.javascript +
        students[3].scores.frontEnd.react +
        students[3].scores.backEnd.java +
        students[3].scores.backEnd.python) /
      4,
    GPA:
      (university.credits.javascript * university.gpa[3] +
        university.credits.react * university.gpa[0.5] +
        university.credits.python * university.gpa[2] +
        university.credits.java * university.gpa[1]) /
      (university.credits.javascript +
        university.credits.python +
        university.credits.java +
        university.credits.react),
  });

// 3. გამოთვალეთ 4 - ვე სტუდენტის საშუალო არითმეტიკული (ანუ ვაფშე ყველა ქულა და ყველა საგანი),
// სტუდენტებს, რომელთა ცალკე საშუალო არითმეტიკული მეტი აქვთ ვიდრე საერთო არითმეტიკული, მივანიჭოთ
// "წითელი დიპლომის მქონეს" სტატუსი, ხოლო ვისაც საერთო საშუალოზე ნაკლები აქვს - "ვრაგ ნაროდა" სტატუსი

const totalAverage =
  (statistics[0].sumOfScores +
    statistics[1].sumOfScores +
    statistics[2].sumOfScores +
    statistics[3].sumOfScores) /
  16;

console.log(
  statistics[0].averageScores <= totalAverage
    ? `${students[0].firstName} Enemy of the People`
    : `${students[0].firstName} with Honours`
);
console.log(
  statistics[1].averageScores <= totalAverage
    ? `${students[1].firstName} Enemy of the People`
    : `${students[1].firstName} with Honours`
);
console.log(
  statistics[2].averageScores <= totalAverage
    ? `${students[2].firstName} Enemy of the People`
    : `${students[2].firstName} with Honours`
);
console.log(
  statistics[3].averageScores <= totalAverage
    ? `${students[3].firstName} Enemy of the People`
    : `${students[3].firstName} with Honours`
);

// 4 უნდა გამოავლინოთ საუკეთესო სტუდენტი GPA ს მიხედვით

let highestGPAStudent = students[0];
let highestGPAScores = statistics[0];

if (statistics[1].GPA > statistics[0].GPA) {
  highestGPAStudent = students[1];
  highestGPAScores = statistics[1];
}
if (statistics[2].GPA > highestGPAScores.GPA) {
  highestGPAStudent = students[2];
  highestGPAScores = statistics[2];
}
if (statistics[3].GPA > highestGPAScores.GPA) {
  highestGPAStudent = students[3];
  highestGPAScores = statistics[3];
}

console.log(`${highestGPAStudent.firstName} is Student with the highest GPA`);

// უნდა გამოავლინოთ საუკეთესო სტუდენტი 21 + ასაკში საშუალო ქულების მიხედვით
// (აქ 21+ იანის ხელით ამორჩევა არ იგულისხმება, უნდა შეამოწმოთ როგორც საშუალო ქულა, ასევე ასაკი)

let gradesOver21 = [];

if (students[0].age >= 21) {
  gradesOver21[0] = statistics[0].averageScores;
} else {
  gradesOver21[0] = 0;
}
if (students[1].age >= 21) {
  gradesOver21[1] = statistics[1].averageScores;
} else {
  gradesOver21[1] = 0;
}
if (students[2].age >= 21) {
  gradesOver21[2] = statistics[2].averageScores;
} else {
  gradesOver21[2] = 0;
}
if (students[3].age >= 21) {
  gradesOver21[3] = statistics[3].averageScores;
} else {
  gradesOver21[3] = 0;
}

let highestGradeOver21 = gradesOver21[0];

if (gradesOver21[1] > highestGradeOver21) {
  highestGradeOver21 = gradesOver21[1];
}
if (gradesOver21[2] > highestGradeOver21) {
  highestGradeOver21 = gradesOver21[2];
}
if (gradesOver21[3] > highestGradeOver21) {
  highestGradeOver21 = gradesOver21[3];
}
console.log(highestGradeOver21);

// 6. უნდა გამოავლინოთ სტუდენტი რომელიც საუკეთესოა ფრონტ-ენდის საგნებში საშუალო ქულების მიხედვით (js, react)

let frontEndKing = students[0];

if (
  students[1].scores.frontEnd.javascript + students[1].scores.frontEnd.react >
  frontEndKing.scores.frontEnd.javascript + frontEndKing.scores.frontEnd.react
) {
  frontEndKing = students[1];
}

if (
  students[2].scores.frontEnd.javascript + students[2].scores.frontEnd.react >
  frontEndKing.scores.frontEnd.javascript + frontEndKing.scores.frontEnd.react
) {
  frontEndKing = students[2];
}

if (
  students[3].scores.frontEnd.javascript + students[3].scores.frontEnd.react >
  frontEndKing.scores.frontEnd.javascript + frontEndKing.scores.frontEnd.react
) {
  frontEndKing = students[3];
}

console.log(frontEndKing);
console.log(university);
console.log(students);
console.log(statistics);
